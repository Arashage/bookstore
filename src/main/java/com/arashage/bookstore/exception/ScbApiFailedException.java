package com.arashage.bookstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FAILED_DEPENDENCY)
public class ScbApiFailedException extends RuntimeException {

    private static final String TEMPLATE = "Can't get data from SCB API";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ScbApiFailedException() {
        super(String.format(TEMPLATE));
    }

}