package com.arashage.bookstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.PARTIAL_CONTENT)
public class BookNotExistException extends RuntimeException {

    private static final String TEMPLATE = "Book ID %d are not found";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public BookNotExistException(String bookIdsString) {
        super(String.format(TEMPLATE, bookIdsString));
    }

}