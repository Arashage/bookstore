package com.arashage.bookstore.service;

import com.arashage.bookstore.domain.User;
import com.arashage.bookstore.domain.UserDetail;

public interface UserService {

    User getUser(User user);

    UserDetail getUserDetail(long userId);

    void saveUser(User user);

    void deleteUser(long userId);

}
