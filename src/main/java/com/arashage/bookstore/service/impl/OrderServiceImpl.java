package com.arashage.bookstore.service.impl;

import com.arashage.bookstore.domain.Book;
import com.arashage.bookstore.domain.Order;
import com.arashage.bookstore.domain.OrderDetail;
import com.arashage.bookstore.exception.BookNotExistException;
import com.arashage.bookstore.repository.BookRepository;
import com.arashage.bookstore.repository.OrderDetailRepository;
import com.arashage.bookstore.repository.OrderRepository;
import com.arashage.bookstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Order createOrder(long userId, List<Long> bookIdList) {

        BigDecimal total = new BigDecimal(0);

        Order newOrder = Order.builder()
                .orderDate(new Date())
                .userId(userId)
                .build();

        List<OrderDetail> newOrderDetailList = new ArrayList<>();
        List<Long> missingBook = new ArrayList<>();

        bookIdList.forEach(bookId -> {
            Book book = Optional.ofNullable(bookRepository.findById(bookId).get()).orElse(null);

            if (book != null) {
                total.add(book.getPrice());

                OrderDetail newOrderDetail = OrderDetail.builder()
                        .orderId(newOrder.getId())
                        .bookId(bookId)
                        .quantity(1)
                        .price(book.getPrice())
                        .build();
                newOrderDetailList.add(newOrderDetail);
            } else {
                missingBook.add(bookId);
            }


        });

        newOrder.setTotal(total);

        if (!missingBook.isEmpty()) {
            throw new BookNotExistException(missingBook.toString());
        }

        orderRepository.save(newOrder);
        orderDetailRepository.saveAll(newOrderDetailList);

        return newOrder;
    }
}
