package com.arashage.bookstore.service.impl;

import com.arashage.bookstore.config.ScbConfig;
import com.arashage.bookstore.constant.RestResources;
import com.arashage.bookstore.domain.Book;
import com.arashage.bookstore.exception.ScbApiFailedException;
import com.arashage.bookstore.service.ScbService;
import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ScbServiceImpl implements ScbService {

    private final ScbConfig scbConfig;
    private final OkHttpClient httpClient;

    @Autowired
    public AmapApiServiceImpl(
            final ScbConfig scbConfig,
            final OkHttpClient httpClient
    ) {
        this.scbConfig = scbConfig;
        this.httpClient = httpClient;
    }

    public interface ScbClient {
        @Headers({"Accept: application/json"})
        @RequestLine("GET {path}")
        List<Book> getBook(
                @Param(value = "path") String path
        );
    }

    @Override
    public List<Book> getBooks() {
        return getBooksGeneric(RestResources.SCB_BOOKS);
    }

    @Override
    public List<Book> getRecommendBook() {
        return getBooksGeneric(RestResources.SCB_BOOKS_RECOMMENDED);
    }

    public List<Book> getBooksGeneric(String path) {
        try {
            ScbClient service = Feign.builder()
                    .decoder(new JacksonDecoder())
                    .client(new feign.okhttp.OkHttpClient(httpClient))
                    .target(ScbClient.class, scbConfig.getUrl());

            List<Book> response = service.getBook(path);
            return Optional.ofNullable(response).orElse(Collections.emptyList());
        } catch (Exception exception) {
            throw new ScbApiFailedException();
        }

    }
}
