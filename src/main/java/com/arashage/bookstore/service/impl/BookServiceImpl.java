package com.arashage.bookstore.service.impl;

import com.arashage.bookstore.domain.Book;
import com.arashage.bookstore.exception.ScbApiFailedException;
import com.arashage.bookstore.repository.BookRepository;
import com.arashage.bookstore.service.BookService;
import com.arashage.bookstore.service.ScbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ScbService scbService;

    @Override
    public List<Book> getBooks() {

        List<Book> bookList = bookRepository.findAll();

        if (bookList.isEmpty() || bookList.get(0).getUpdateDateTime().compareTo(new Date()) >= 7) {
            updateBooks();
            bookList = bookRepository.findAll();
        }

        return bookList;
    }

    @Override
    public void updateBooks() {

        try {
            List<Book> allBookList = scbService.getBooks();
            List<Book> recommendedBookList = scbService.getRecommendBook();

            allBookList.forEach(book -> {
                if (recommendedBookList.contains(book)) {
                    book.setRecommended(true);
                }
            });

            bookRepository.deleteAll();
            bookRepository.saveAll(allBookList);
        } catch (Exception exception) {
            throw new ScbApiFailedException();
        }


    }

}
