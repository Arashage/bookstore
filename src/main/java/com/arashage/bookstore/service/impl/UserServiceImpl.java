package com.arashage.bookstore.service.impl;

import com.arashage.bookstore.domain.Order;
import com.arashage.bookstore.domain.OrderDetail;
import com.arashage.bookstore.domain.User;
import com.arashage.bookstore.domain.UserDetail;
import com.arashage.bookstore.repository.OrderDetailRepository;
import com.arashage.bookstore.repository.OrderRepository;
import com.arashage.bookstore.repository.UserRepository;
import com.arashage.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getUser(User user) {
        return userRepository.findByUsernameAndPassword(user.getUsername(), bCryptPasswordEncoder.encode(user.getPassword())).get();
    }

    @Override
    public UserDetail getUserDetail(long userId) {
        User user = Optional.ofNullable(userRepository.findById(userId).get()).orElse(null);

        if (user == null) {
            return null;
        }

        String[] fullName = user.getUsername().split(".");
        List<Order> orderList = Optional.ofNullable(orderRepository.findAllByUserId(user.getId()).get()).orElse(null);
        List<Long> bookList = new ArrayList<>();

        orderList.forEach(order -> {
            List<OrderDetail> orderDetailList = Optional.ofNullable(orderDetailRepository.findAllByOrderId(order.getId()).get()).orElse(null);
            orderDetailList.forEach(orderDetail -> {
                bookList.add(orderDetail.getBookId());
            });
        });

        return UserDetail.builder()
                .name(Optional.ofNullable(fullName[0]).orElse(""))
                .surname(Optional.ofNullable(fullName[1]).orElse(""))
                .birthDate(user.getBirthDate())
                .bookList(bookList)
                .build();
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }
}
