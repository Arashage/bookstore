package com.arashage.bookstore.service;

import com.arashage.bookstore.domain.Book;

import java.util.List;

public interface BookService {

    List<Book> getBooks();

    void updateBooks();

}
