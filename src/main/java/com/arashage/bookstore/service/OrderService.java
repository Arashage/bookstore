package com.arashage.bookstore.service;

import com.arashage.bookstore.domain.Order;

import java.util.List;

public interface OrderService {

    Order createOrder(long userId, List<Long> bookIdList);

}
