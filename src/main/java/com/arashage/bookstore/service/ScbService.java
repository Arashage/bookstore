package com.arashage.bookstore.service;

import com.arashage.bookstore.domain.Book;

import java.util.List;

public interface ScbService {

    List<Book> getBooks();

    List<Book> getRecommendBook();

}
