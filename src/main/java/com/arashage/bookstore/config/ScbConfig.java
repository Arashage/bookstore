package com.arashage.bookstore.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties(prefix = "scb")
@Configuration
@Data
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class ScbConfig {

    @NotEmpty
    private String url;

}
