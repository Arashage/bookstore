package com.arashage.bookstore.resource;

import com.arashage.bookstore.constant.RestResources;
import com.arashage.bookstore.domain.Book;
import com.arashage.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class BookResource {

    @Autowired
    private BookService bookService;

    @GetMapping(value = RestResources.BOOKS)
    public ResponseEntity getBooks() {
        try {
            List<Book> booksList = bookService.getBooks();
            return ResponseEntity.ok(booksList);
        } catch (Exception exception) {
            return null;
        }
    }

}
