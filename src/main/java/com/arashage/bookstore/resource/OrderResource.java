package com.arashage.bookstore.resource;

import com.arashage.bookstore.constant.RestResources;
import com.arashage.bookstore.domain.Order;
import com.arashage.bookstore.service.OrderService;
import com.arashage.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class OrderResource {

    @Autowired
    private OrderService orderService;

    @PostMapping(value = RestResources.ORDERS)
    @PreAuthorize("isAnonymous()")
    public ResponseEntity createOrder(@PathVariable final long userId, @RequestBody List<Long> bookIdList) {

        try {
            orderService.createOrder(userId, bookIdList);
            return ResponseEntity.ok().build();
        } catch (Exception exception) {
            return null;
        }

    }

}
