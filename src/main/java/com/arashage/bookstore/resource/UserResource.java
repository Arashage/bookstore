package com.arashage.bookstore.resource;

import com.arashage.bookstore.constant.RestResources;
import com.arashage.bookstore.domain.User;
import com.arashage.bookstore.domain.UserDetail;
import com.arashage.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserResource {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @PostMapping(value = RestResources.LOGIN)
    public ResponseEntity login(@RequestBody User data) {
        try {
            String username = data.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
            //String token = jwtTokenProvider.createToken(username, this.users.findByEmail(username).getRoles());

            // User Token provider to generate or user config file and redirect to localhost:8080/oauth/token -d

            Map<Object, Object> model = new HashMap<>();
            model.put("username", username);
            //model.put("token", token);
            return ResponseEntity.ok(model);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid email/password supplied");
        }
    }

    @GetMapping(value = RestResources.USERS)
    @PreAuthorize("isAnonymous()")
    public ResponseEntity getUser(@PathVariable final long userId) {
        UserDetail userDetail = userService.getUserDetail(userId);

        if (userDetail != null) {
            return ResponseEntity.ok(userDetail);
        } else {
            return null;
        }
    }

    @PostMapping(value = RestResources.POST_USERS)
    public ResponseEntity createUser(@RequestBody User user) {
        try {
            userService.saveUser(user);
            return ResponseEntity.ok().build();
        } catch (Exception exception) {
            return null;
        }
    }

    @DeleteMapping(value = RestResources.USERS)
    @PreAuthorize("isAnonymous()")
    public ResponseEntity deleteUser(@PathVariable final long userId) {
        try {
            userService.deleteUser(userId);
            return ResponseEntity.ok().build();
        } catch (Exception exception) {
            return null;
        }
    }

}
