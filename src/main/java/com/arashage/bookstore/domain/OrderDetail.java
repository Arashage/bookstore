package com.arashage.bookstore.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "ORDER_DETAIL")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID")
    private long id;

    @Column(name = "ORDER_ID")
    private long orderId;

    @Column(name = "BOOK_ID")
    private long bookId;

    @Column(name = "QUANTITY")
    private long quantity;

    @Column(name = "PRICE")
    private BigDecimal price;

}
