package com.arashage.bookstore.domain;

import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserDetail {

    private String name;
    private String surname;
    private Date birthDate;
    private List<Long> bookList;

}
