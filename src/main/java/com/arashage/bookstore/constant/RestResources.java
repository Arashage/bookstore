package com.arashage.bookstore.constant;

public final class RestResources {

    public static final String LOGIN = "/login";
    public static final String USERS = "/users/{userId}";
    public static final String POST_USERS = "/register";
    public static final String ORDERS = USERS + "/orders";
    public static final String BOOKS = "/books";

    public static final String SCB_BOOKS = "/books";
    public static final String SCB_BOOKS_RECOMMENDED = SCB_BOOKS + "/recommendation";

}
