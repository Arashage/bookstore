package com.arashage.bookstore.service;

import com.arashage.bookstore.domain.User;
import com.arashage.bookstore.domain.UserDetail;
import com.arashage.bookstore.repository.BookRepository;
import com.arashage.bookstore.repository.UserRepository;
import com.arashage.bookstore.service.impl.BookServiceImpl;
import com.arashage.bookstore.service.impl.UserServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import sun.java2d.pipe.SpanShapeRenderer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void testConfigRepositoryIsNotNull() {
        Assertions.assertThat(userRepository).isNotNull();
    }

    @Test
    public void testGetUserDetail() {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String stringDate = "31/12/1998";

        Date date = new Date();
        try {
           date = format.parse(stringDate);
        } catch (ParseException exception) {

        }

        User user = User.builder()
                .id(1)
                .username("John.Doe")
                .password("TestABC")
                .birthDate(date)
                .build();

        userService.saveUser(user);

        UserDetail expected = UserDetail.builder()
                .name("John")
                .surname("Doe")
                .birthDate(date)
                .build();
        UserDetail actual = userService.getUserDetail(1);

        Assertions.assertThat(actual.getName()).isEqualToIgnoringCase(expected.getName());
        Assertions.assertThat(actual.getSurname()).isEqualToIgnoringCase(expected.getSurname());
        Assertions.assertThat(actual.getBirthDate()).isEqualTo(expected.getBirthDate());
    }

}
