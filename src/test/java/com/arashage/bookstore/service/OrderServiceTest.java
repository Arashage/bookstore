package com.arashage.bookstore.service;

import com.arashage.bookstore.repository.BookRepository;
import com.arashage.bookstore.repository.OrderDetailRepository;
import com.arashage.bookstore.repository.OrderRepository;
import com.arashage.bookstore.service.impl.BookServiceImpl;
import com.arashage.bookstore.service.impl.OrderServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class OrderServiceTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderDetailRepository orderDetailRepository;

    @Test
    public void testConfigRepositoryIsNotNull() {
        Assertions.assertThat(orderRepository).isNotNull();
        Assertions.assertThat(orderDetailRepository).isNotNull();
    }

    @Test
    public void testCreateOrder() {
    }

}
