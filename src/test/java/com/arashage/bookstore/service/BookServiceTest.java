package com.arashage.bookstore.service;

import com.arashage.bookstore.repository.BookRepository;
import com.arashage.bookstore.service.impl.BookServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BookServiceTest {

    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private BookRepository bookRepository;

    @Test
    public void testConfigRepositoryIsNotNull() {
        Assertions.assertThat(bookRepository).isNotNull();
    }

    @Test
    public void testGetBooksAtLeastOne() {
        Assertions.assertThat(bookService.getBooks()).isNotEmpty();
    }

}
